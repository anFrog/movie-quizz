import React from 'react';
import './App.css';
import SceneSelector from './components/Scenes/SceneSelector';
import Grid from '@material-ui/core/Grid';
import { ThemeProvider } from '@material-ui/styles';
import { theme } from './MUI-Theme'

function App() {
    return (
        <ThemeProvider theme={theme}>
            <Grid
                container
                spacing={0}
                align="center"
                justify="center"
                style={{ height: '100vh', 'alignContent': 'center' }}
            >
                <SceneSelector />
            </Grid>
        </ThemeProvider>
    );
}

export default App;
