import { createMuiTheme } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';
import deepPurple from '@material-ui/core/colors/deepPurple';
import yellow from '@material-ui/core/colors/yellow';
import grey from '@material-ui/core/colors/grey';

export const theme = createMuiTheme({
    palette: {
        primary: { light: deepPurple[400], main: deepPurple[600], dark: deepPurple[800] },
        success: { light: green[700], main: green[800], dark: green[900] },
        error: { light: red[700], main: red[800], dark: red[900] },
        grey: { light: grey[600], main: grey[800], dark: '#333', darker: '#212121' },
        gold: { light: '#AF9500', main: yellow[800], dark: yellow[900] },
        silver: '#D7D7D7',
        bronze: '#6A3805'
    },
    overrides: {
        MuiButton: {
            root: {
                margin: "10px",
                padding: "10px"
            }
        },
        MuiPaper: {
            root: {
                margin: "10px"
            }
        }
    }
});