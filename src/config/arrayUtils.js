export const arraySlide = array => {
    let slices = [array.slice(0, array.length / 2), array.slice(array.length / 2, array.length)],
    first = slices[1].shift();

    slices[1] = [...slices[1], first]
    
    return [...slices[0], ...slices[1]];
};

export const arrayShuffle = array => {
    let shuffled = [];
    const arrayLength = array.length;

    while(shuffled.length < arrayLength) {
        const index = Math.floor(Math.random() * arrayLength);

        if(shuffled.indexOf(array[index]) === -1) {
            shuffled = [...shuffled, array[index]];
        }
    }

    return shuffled;
};

export const randomElement = array => array[Math.floor(Math.random() * array.length)];