import { useState, useEffect } from 'react';
import axios from 'axios';
import API from '../config/TmDB-API';
import { setMovies, setActors, setQuestions } from './hooksUtils';

const useMoviesFetch = (page) => {
    const [data, setDatas] = useState([]);

    useEffect(() => {
        const fetchAPI = async () => {
            const popularMovies = await axios.get(`${ API.base_url }movie/popular?api_key=${ API.key }&language=fr-FR&page=${ page }&region=FR`),
            allMovies = await Promise.all(popularMovies.data.results.map(item => axios.get(`${ API.base_url }movie/${ item.id }?api_key=${ API.key }&language=fr-FR&append_to_response=credits`))),
            poster_placeholder =  `https://via.placeholder.com/300x450.png/09f/fff`,
            movies = setMovies(allMovies, poster_placeholder),
            actors = setActors(allMovies),
            questions = setQuestions(movies, actors, poster_placeholder);
            
            setDatas(questions)
        }
        fetchAPI();
    }, [page])

    return [data, setDatas];
}

export default useMoviesFetch;
