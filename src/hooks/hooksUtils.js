import { arraySlide, arrayShuffle, randomElement } from '../config/arrayUtils';

export const setMovies = (response, placeholder) => {
    return response.map(item => {
        return {
            title: item.data.title,
            poster: (item.data.poster_path !== null) ? `http://image.tmdb.org/t/p/w300${ item.data.poster_path }` : placeholder + `?text=${ item.data.title.replace(/ /g, '+') }`,
            overview: item.data.overview
        };
    });
};

export const setActors = (response) => {
    return arraySlide(response.map(item => randomElement(item.data.credits.cast)));
};

export const setQuestions = (movies, actors, placeholder) => {
    return arrayShuffle(movies.map((item, index) => {
        return {
            movie: item,
            actor: { 
                name: actors[index].name,
                poster: (actors[index].profile_path !== null) ? `http://image.tmdb.org/t/p/w300${ actors[index].profile_path }` : placeholder + `?text=${ actors[index].name.replace(/ /g, '+') }`
            },
            correctAnswer: (index >= (actors.length - 1) / 2) ? false : true
        };
    }));
};
