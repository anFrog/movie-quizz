import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
    root: {
        height: '100%',
        alignContent: 'center',
        color: theme.palette.grey.light,
    },
    paper: {
        backgroundColor: theme.palette.grey.dark,
        padding: 10
    },
    gold: {
        color: theme.palette.gold.main
    },
    gameover: {
        color: theme.palette.error.main
    },
    textField: {
        '& label, & .MuiInput-underline, &MuiFormLabel-root.Mui-focused':{
            color: theme.palette.primary.light,
        },
        '& .MuiInput-underline:after, & .MuiInput-underline:before, & label.Mui-focused, .MuiInput-underline:hover:not(.Mui-disabled):before': {
            color: theme.palette.primary.light,
            borderBottomColor: theme.palette.primary.light,
        }
    },
    iconButton: {
        color: theme.palette.primary.light
    },
    play: {
        padding: 50
    }
}));
