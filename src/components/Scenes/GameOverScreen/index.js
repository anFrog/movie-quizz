import React, { useState } from 'react';
import { connect } from 'react-redux';
import { debounce } from 'lodash';
import HighScores from '../../HighScores';
import GameButton from '../../GameButton';
import { setScene } from '../../../redux/actions/';
import { Grid, Paper, Typography, TextField, IconButton } from '@material-ui/core/';
import AddBoxIcon from '@material-ui/icons/AddBox';
import { useStyles } from './GameOverScreen-style';

const GameOverScreen = ({ correctAnswers, timer, setScene }) => {
    const initialScoreTable = () => JSON.parse(localStorage.getItem('highScores')) || [],
    [scoresTable, setScoreTable] = useState(initialScoreTable),
    [scoreSent, sendScore] = useState(false),
    [name, setPlayerName] = useState(''),
    classes = useStyles(),

    handleChange = debounce((value) => {
        setPlayerName(value)
    }, 1000),

    handleClick = () => {
        let newScores = [...scoresTable, { name, correctAnswers, timer }]
        .sort((a,b) => b.correctAnswers - a.correctAnswers)
        .slice(0, 10);

        localStorage.setItem('highScores', JSON.stringify(newScores));
        setScoreTable(scoresTable => newScores)
        sendScore(prev => !prev)
    },
    replayHandler = e => {
        setScene(1);
    }

    return (
        <Grid container justify="center" className={ classes.root }>
            <Grid container justify="center" className={ classes.playerRecap }>
                <Grid item xs={ 12 }>
                    <Paper className={ classes.paper }>
                        <Typography variant="h3" className={ classes.gameover }>
                            Game over
                        </Typography>
                        <Typography variant="subtitle1" className={ classes.gold }>
                            { correctAnswers } bonnes réponses en { (timer.minutes > 0) && `${ timer.minutes } minutes et ` } { `${ timer.secondes } secondes.` }
                        </Typography>
                        <GameButton text="Rejouer" onClick={ replayHandler } color="primary"/>
                    </Paper>
                </Grid>
                {(correctAnswers > 0 && !scoreSent 
                && (scoresTable.length < 10 || scoresTable.find((item) => item.correctAnswers < correctAnswers) )) && (
                    <Grid item xs={ 6 }>
                        <Paper className={ classes.paper }>
                            <Typography variant="subtitle1" className={ classes.root }>Ajouter aux High scores ?</Typography>
                            <TextField
                                id="standard-name"
                                label="Nom"
                                className={classes.textField}
                                onChange={ e => handleChange(e.target.value) }
                                margin="none"
                            />
                            <IconButton className={classes.iconButton} aria-label="add"  onClick={ e => handleClick(e) } disabled={ !name }>
                                <AddBoxIcon fontSize="large"/>
                            </IconButton>
                        </Paper>
                    </Grid>
                )}
            </Grid>
            <Grid container justify="center" className={ classes.scores }>
                <HighScores scores={ scoresTable }/>
            </Grid>
        </Grid>
    );
}

const mapStateToProps = state => ({
    correctAnswers: state.correctAnswers,
    timer: state.recapTimer
});

const mapDispatchToProps = dispatch => {
    return {
        setScene: scene => dispatch(setScene(scene))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(GameOverScreen);
