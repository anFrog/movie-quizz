import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Timer from '../../Timer';
import PlayerAnswers from '../../PlayerAnswers';
import Actor from '../../Actor';
import Movie from '../../Movie';
import GameButton from '../../GameButton';
import useMoviesFetch from '../../../hooks/useMoviesFetch';
import { setQuestions, updatePopularMoviesPage, setScene, setCorrectAnswers, setRecapTimer } from '../../../redux/actions/';
import { Grid, Paper, Typography } from '@material-ui/core/';
import { useStyles } from './GameScreen-style';

const GameScreen = ({ correctAnswers, questions, setQuestions, popularMoviesPage, updatePopularMoviesPage, setCorrectAnswers, setScene, setRecapTimer }) => {
    const [page, setPage] = useState(popularMoviesPage + 1),
    [newQuestions, addNewQuestions] = useMoviesFetch(page),
    [timer, setTimer] = useState({}),
    classes = useStyles();

    const updateQuestions = questions => {
        if (questions.length < 3){
            setPage(prev => prev + 1)
            updatePopularMoviesPage(page);
            addNewQuestions(page);
            setQuestions([...questions, ...newQuestions]);
        } else {
            setQuestions(questions);
        }
    }

    const handleAnswer = (e, userAnswer) => {
        let questionsWithoutLastAnswer = questions.slice(1, questions.length);
        if (userAnswer === questions[0].correctAnswer){
            setCorrectAnswers(correctAnswers + 1);
        } else {
            setRecapTimer(timer);
            setScene(2);
        }
        updateQuestions(questionsWithoutLastAnswer)
    };

    useEffect(() => {
        setCorrectAnswers(0);
    }, [setCorrectAnswers])

    return (
            <Grid container className={ classes.root }>
                <Grid container className={ classes.playerInfos } >
                    <Paper className={ classes.paper } xs={ 12 } md={ 6 }>
                        <Timer timer={ setTimer } />
                    </Paper>
                    <Paper className={ classes.paper } xs={ 12 } md={ 6 }>
                        <PlayerAnswers answers={ correctAnswers } />
                    </Paper>
                </Grid>
                <Grid container justify="center" className={ classes.quizzInfos } p={ 2 }>
                    <Grid item xs={ 12 } sm={ 4 }>
                        <Actor actor={ questions[0].actor } />
                    </Grid>
                    <Grid item xs={ 12 } sm={ 4 }>
                        <Movie movie={ questions[0].movie } />
                    </Grid>
                </Grid>
                <Grid container justify="space-evenly" className={ classes.answers }>
                    <Grid item xs={ 12 }>
                        <Typography >{ questions[0].actor.name } joue dans { questions[0].movie.title }</Typography>
                        <GameButton color="success" text="Vrai" onClick={ (e) => handleAnswer(e, true) }/>
                        <GameButton color="error" text="Faux" onClick={ (e) => handleAnswer(e, false) }/>
                    </Grid>
                </Grid>
            </Grid>
    );
};

const mapStateToProps = state => ({
    correctAnswers: state.correctAnswers,
    questions: state.questions,
    popularMoviesPage: state.popularMoviesPage,
    recapTimer: state.recapTimer
});

const mapDispatchToProps = dispatch => {
    return {
        updatePopularMoviesPage: page => dispatch(updatePopularMoviesPage(page)),
        setCorrectAnswers: number => dispatch(setCorrectAnswers(number)),
        setQuestions: questions => dispatch(setQuestions(questions)),
        setScene: scene => dispatch(setScene(scene)),
        setRecapTimer: timer => dispatch(setRecapTimer(timer)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(GameScreen);
