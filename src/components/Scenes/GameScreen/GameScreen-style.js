import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
    root: {
        'height': '100%'
    },
    paper: {
        backgroundColor: theme.palette.grey.darker
    },
    playerInfos: {
        backgroundColor: theme.palette.grey.dark,
        padding: '10px 20px',
        [theme.breakpoints.down('sm')]: {
            justifyContent: 'center'
        },
        [theme.breakpoints.up('sm')]: {
            justifyContent: 'flex-end',
            alignItems: 'center'
        }
    },
    quizzInfos: {
        backgroundColor: theme.palette.grey.darker,
        [theme.breakpoints.up('sm')]: {
            alignItems: 'center'
        },
        '& > .MuiGrid-item': {
            maxWidth: 400
        }
    },
    answers: {
        backgroundColor: theme.palette.grey.dark,
        alignItems: 'center'
    }
}));
