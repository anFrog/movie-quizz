import React from 'react';
import { connect } from 'react-redux';
import GameButton from '../../GameButton';
import useMoviesFetch from '../../../hooks/useMoviesFetch';
import { setScene, setQuestions } from '../../../redux/actions/';
import { Container, Typography } from '@material-ui/core';
import { useStyles } from './StartScreen-style';

const StartScreen = ({ setQuestions, setScene }) => {
    const [questions] = useMoviesFetch(1),
    classes = useStyles();

    function onClickHandler(e) {
        setQuestions(questions);
        setScene(1);
    }

    return (
        <Container className={ classes.start } fixed>
            <Typography variant="h3" className={ classes.title }>Movie Quizz</Typography>
            <GameButton text="Play !" className={ classes.play } onClick={ onClickHandler } color="primary"/>
        </Container>
    );
};

const mapStateToProps = state => ({
    popularMoviesPage: state.popularMoviesPage
});

const mapDispatchToProps = dispatch => {
    return {
        setScene: scene => dispatch(setScene(scene)),
        setQuestions: questions => dispatch(setQuestions(questions))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(StartScreen);
