import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
    title: {
        margin: '5vh 0',
        color: theme.palette.primary.light
    },
    start: {
        padding: '100px 50px',
        background: theme.palette.grey.dark
    },
    play: {
        padding: 50
    }
}));
