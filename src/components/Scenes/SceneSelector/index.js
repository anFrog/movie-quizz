import React from 'react';
import { connect } from 'react-redux';
import StartScreen from '../StartScreen';
import GameScreen from '../GameScreen';
import GameOverScreen from '../GameOverScreen';

function SceneSelector({ gameScene }) {
    switch (gameScene) {
        case 0:
            return <StartScreen />;
        case 1:
            return <GameScreen />;
        case 2:
            return <GameOverScreen />;
        default:
            return <StartScreen />;
    }
}

const mapStateToProps = state => ({
    gameScene: state.gameScene
})

export default connect(mapStateToProps)(SceneSelector);