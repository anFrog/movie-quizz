import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
    root: {
        fontSize: '1rem',
        lineHeight: 2,
        color: theme.palette.error.light,
        padding: '10px 20px'
    }
}));
