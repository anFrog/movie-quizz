import React, { useState, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import { setRecapTimer, resetRecapTimer } from '../../redux/actions/';
import Typography from '@material-ui/core/Typography';
import { useStyles } from './Timer-style';

function Timer({ timer }) {
    const [secondes, setSecondes] = useState(0);
    const minutes = useRef(0);
    const classes = useStyles();

    useEffect(() => {
        let interval = null;
            interval = setInterval(() => {
                setSecondes(secondes => secondes + 1);
            }, 1000);
        return () => clearInterval(interval);
    }, [])

    useEffect(() => {
        (secondes >= 60) && setSecondes(0);
        (secondes === 60) && minutes.current++;
    }, [secondes])

    useEffect(() => {
        timer({ minutes: minutes.current, secondes })
    }, [secondes, timer])

    return (
        <Typography className={ classes.root }>
            Temps: { minutes.current }:{ (secondes < 10) ? `0${secondes}` : secondes }
        </Typography>
    )
}

const mapStateToProps = state => ({
    gameScene: state.gameScene,
    recapTimer: state.recapTimer
})

const mapDispatchToProps = dispatch => {
    return {
        setRecapTimer: timer => dispatch(setRecapTimer(timer)),
        resetRecapTimer: () => dispatch(resetRecapTimer())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Timer);
