import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
    card: {
        background: theme.palette.grey.dark,
    },
    title: {
        fontSize: '1rem',
        margin: '10px 0',
        color: theme.palette.success.light
    },
    media: {
    }
}));
