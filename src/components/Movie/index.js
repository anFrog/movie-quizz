import React from 'react';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import { useStyles } from './Movie-style';

const Movie = ({ movie }) => {
    const classes = useStyles();

    return (
        <Card className={ classes.card }>
            <CardMedia
                component="img"
                className={ classes.media }
                image={ movie.poster }
                title={ movie.title }
            />
            <CardContent>
                <Typography className={ classes.title }>
                    { movie.title }
                </Typography>
            </CardContent>
        </Card>
    );
};

export default Movie;
    