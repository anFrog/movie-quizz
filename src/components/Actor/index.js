import React from 'react';
import { Typography, Card, CardMedia, CardContent } from '@material-ui/core/';
import { useStyles } from './Actor-style';

const Actor = ({ actor }) => {
    const classes = useStyles();

    return (
        <Card className={ classes.card }>
            <CardMedia
                component="img"
                className={ classes.media }
                image={ actor.poster }
                title={ actor.name }
            />
            <CardContent>
                <Typography className={ classes.title }>
                    { actor.name }
                </Typography>
            </CardContent>
        </Card>
    );
};

export default Actor;
