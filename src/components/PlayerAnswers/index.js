import React from 'react';
import Typography from '@material-ui/core/Typography';
import { useStyles } from './PlayerAnswers-style';

const PlayerAnswers = ({ answers }) => {
    const classes = useStyles();

    return (
        <Typography className={ classes.root }>
            Bonnes réponses : { answers }
        </Typography>
    )
}

export default PlayerAnswers;
