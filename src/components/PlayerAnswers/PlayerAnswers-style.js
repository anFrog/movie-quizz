import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
    root: {
        fontSize: '1rem',
        lineHeight: 2,
        color: theme.palette.gold.main,
        padding: '10px 20px'
    }
}));
