import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
    root: {
        '& .MuiToolbar-root': {
            color: theme.palette.gold.main,
            background: theme.palette.grey.darker,
            borderRadius: '4px 4px 0 0',
            textAlign: 'center'
        },
        '& .MuiPaper-root': {
            padding: 4,
            background: theme.palette.grey.dark
        }        
    },
    'table': {
        '& thead th.MuiTableCell-head': {
            color: theme.palette.grey.light
        },
        '& tbody tr > *': {
            color: "#ababab"
        }
    }
}));
