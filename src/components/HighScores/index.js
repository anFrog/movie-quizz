import React from 'react';
import { Grid, Table, TableBody, TableCell, TableHead, TableRow, Paper, Toolbar  } from '@material-ui/core/';
import { useStyles } from './HighScores-style';

const HighScores = ({ scores }) => {
    const classes = useStyles();
    
    return (
        (scores && scores.length > 0) ? (
            <Grid container justify="center" className={ classes.root }>
                <Grid item xs={ 6 }>
                    <Paper>
                    <Toolbar>High scores</Toolbar>
                        <Table className={ classes.table } size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Nom</TableCell>
                                    <TableCell align="right">Score</TableCell>
                                    <TableCell align="right">Temps</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                            {scores.map((item, index) => (
                                <TableRow key={ item.name }>
                                    <TableCell component="th" scope="row">{ item.name }</TableCell>
                                    <TableCell align="right">{ item.correctAnswers }</TableCell>
                                    <TableCell align="right">{ (item.timer.minutes > 0) && `${ item.timer.minutes }min. ` } { `${ item.timer.secondes}sec.` }</TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                    </Paper>
                </Grid>
            </Grid>
        ) : (
            <div>Aucun High Score</div>
        )
    )
};
                
export default HighScores;
                