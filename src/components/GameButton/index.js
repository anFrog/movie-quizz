import React from 'react';
import { connect } from 'react-redux';
import { Button } from '@material-ui/core';
import { useStyles } from './GameButton-style';

const GameButton = ({ onClick, text, color }) => {
    const classes = useStyles();
    
    return (
        <Button variant="outlined" size="large" type="button" className={ `${ classes.root } ${ classes[`btn-${color}`] }` } onClick={ onClick }>{ text }</Button>
    );
};

const mapStateToProps = state => ({
    questions: state.questions
});

export default connect(mapStateToProps)(GameButton);
