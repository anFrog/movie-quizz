import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
    'root': {
        transition: 'all 0.2s ease-in-out',
        '&:hover': {
            borderColor: theme.palette.gold.light,
            color: theme.palette.gold.light
        }
    },
    'btn-gold': {
        color: theme.palette.gold.light,
        borderColor: theme.palette.gold.light,
    },
    'btn-primary': {
        color: theme.palette.primary.light,
        borderColor: theme.palette.primary.light,
    },
    'btn-error': {
        color: theme.palette.error.light,
        borderColor: theme.palette.error.light,
    },
    'btn-success': {
        color: theme.palette.success.light,
        borderColor: theme.palette.success.light,
    }
}));
