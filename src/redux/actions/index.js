export const updatePopularMoviesPage = page => {
    return {
        type: 'UPDATE_MOVIES_PAGE',
        page
    }
};

export const setQuestions = questions => {
    return {
        type: 'SET_QUESTIONS',
        questions
    }
};

export const setRecapTimer = timer => {
    return {
        type: 'SET_RECAP_TIMER',
        timer
    }
};

export const resetRecapTimer = timer => {
    return {
        type: 'RESET_RECAP_TIMER',
        timer
    }
};

export const setScene = scene => {
    return {
        type: 'SET_SCENE',
        scene
    }
};

export const setCorrectAnswers = number => {
    return {
        type: 'SET_CORRECT_ANSWERS',
        number
    }
};
