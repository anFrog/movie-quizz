export const setScene = action => {
    const { scene } = action;
    return {
        gameScene: { $set: scene }
    }
}

export const setCorrectAnswers = action => {
    const { number } = action;
    return {
        correctAnswers: { $set: number }
    }
}