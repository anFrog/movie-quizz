import update from 'react-addons-update';
import { setQuestions } from './questionsReducer';
import { setScene, setCorrectAnswers } from './sceneReducer';
import { setRecapTimer, resetRecapTimer } from './recapTimerReducer';
import { updatePopularMoviesPage } from './moviesReducer';


const INITIAL_STATE = {
    correctAnswers: 0,
    questions: null,
    popularMoviesPage: 1,
    gameScene: 0,
    recapTimer: null
};

const rootReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'SET_QUESTIONS':
            return update(state, setQuestions(action));
        case 'SET_CORRECT_ANSWERS':
            return update(state, setCorrectAnswers(action));
        case 'SET_SCENE':
            return update(state, setScene(action));
        case 'SET_RECAP_TIMER':
            return update(state, setRecapTimer(action));
        case 'RESET_RECAP_TIMER':
            return update(state, resetRecapTimer(action));
        case 'UPDATE_MOVIES_PAGE':
            return update(state, updatePopularMoviesPage(action));
        default:
            return state
    }
}

export default rootReducer;
