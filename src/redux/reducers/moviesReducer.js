export const updatePopularMoviesPage = action => {
    const { page } = action;
    return {
        popularMoviesPage: { $set: page }
    }
};
