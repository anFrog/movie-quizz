export const setRecapTimer = action => {
    const { timer } = action;
    return {
        recapTimer: { $set: timer }
    }
}

export const resetRecapTimer = action => {
    return {
        recapTimer: { $set: 0 }
    }
}