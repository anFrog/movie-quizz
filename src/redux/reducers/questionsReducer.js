export const setQuestions = action => {
    const { questions } = action;
    return {
        questions: { $set: questions }
    }
};
